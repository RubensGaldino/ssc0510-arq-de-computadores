# SSC0510 Arquitetura de Computadores - Arquitetura do SNES
Projeto destinado à apresentar a arquitetura do Super Nintendo e seu funcionamento.

# Slides da Apresentação do Projeto:
[Apresentação da Arquitetura do SNES - Slides](https://docs.google.com/presentation/d/1G7f635qb5jYVZInKTsmL-jTKrruf9wAB3GashPagumU/edit#slide=id.g10cc865d502_0_6)

# Link do Vídeo contendo a explicação do grupo
[Vídeo da Apresentação da Arquiteutra do SNES - Vídeo](https://drive.google.com/file/d/1bGlqImYa3cLPcIJp55ezXOHuez8o35Bo/view?usp=sharing)
